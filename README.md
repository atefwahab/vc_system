### Blockchain Group VC System Task ###

# this was a task by Blockchain group hiring team as below#

### Requirements ###

### Functional Requirements: ###
● Users must be able to create an account. It must contain a username, email, and password
● Authentication is not needed. Simply mock authentication by allowing a user id to be passed in each request in a header
● Users must have a virtual currency property (decimal number; VC henceforth)
● All users must accrue 0.25 VC every 30 minutes
● A user must be able to send any amount of their current VC to up to 10 different users at a time
● A user must be able to retrieve a list of VC transactions where they can see who they’ve sent and received money from


### Project Requirements: ###
● Must be written using the Spring Boot 2.2.1 release
● Must follow code standards, code must be clean, documented and readable
● Must sensibly use Spring Boot features to accomplish the above tasks in the most maintainable manner
● Must develop the project in a development branch (ie. dev/initial), then merge into master once complete. Tag the finished release as v1.0.0-
dev. If you need to fix things afterwards, do so following semver. Delete the old branch on remote
● Write a MySQL script to create any necessary tables


### Where to host the project: ###
Push the project to a private BitBucket repo and add the following as read-only viewers:
● anson.ciurcovich@blockchaingroup.io
● eder.rodrigues@blockchaingroup.io
● rafeh.hulays@blockchaingroup.io 

### What this repository has ? ###

* documentation and manual for how to use system property
* Script to load database for you 
* Spring boot project  web + reactjs + spring data jpa + MySql Connector


### How do I get set up? ###

* run database script
* edit **application.properties** with database credentials
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Who do I talk to? ###

* Mohamed Atef Mohamed *
* atefwahab@gmail.com *