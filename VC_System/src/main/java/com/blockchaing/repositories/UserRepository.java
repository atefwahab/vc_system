package com.blockchaing.repositories;

import org.springframework.data.repository.CrudRepository;

import com.blockchaing.entities.User;

/**
 * 
 * @author atefwahab
 *
 */
public interface UserRepository extends CrudRepository<User, Long> {
	
	public User findUserByUsername(String username);
	public Iterable<User> findByUserIdNot(Long userId);

}
