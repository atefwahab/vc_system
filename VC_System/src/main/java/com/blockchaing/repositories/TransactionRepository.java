/**
 * 
 */
package com.blockchaing.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.blockchaing.entities.Transaction;

/**
 * @author atefwahab
 *
 */
public interface TransactionRepository extends CrudRepository<Transaction, Long> {

	@Query(value = "select * from transactions where from_user_id = ?1 order by transaction_date desc", nativeQuery = true)
	public Iterable<Transaction> findTransactionByFromUserIdSorted(Long fromUserId);

	@Query(value = "select * from transactions where to_user_id = ?1 order by transaction_date desc", nativeQuery = true)
	public Iterable<Transaction> findTransactionByToUserIdSorted(Long toUserId);

}
