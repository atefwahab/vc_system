package com.blockchaing.entities;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * 
 * @author atefwahab
 *
 */
@Entity
@Table(name = "transactions")
public class Transaction implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long transactionId;
	private double transactionAmount;
	private Date transactionDate;
	@ManyToOne
	@JoinColumn(name = "from_user_id", referencedColumnName = "userId")
	private User fromUserId;

// @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
// @JoinColumn(name = "to_user_id")
	@ManyToOne
	@JoinColumn(name = "to_user_id", referencedColumnName = "userId")
	private User toUserId;
	private String transactionNotes;

	protected Transaction(long transactionId, double transactionAmount, Date transactionDate, User fromUser,
			User toUser, String transactionNotes) {
		super();
		this.transactionId = transactionId;
		this.transactionAmount = transactionAmount;
		this.transactionDate = transactionDate;
		this.fromUserId = fromUser;
		this.toUserId = toUser;
		this.transactionNotes = transactionNotes;
	}

	protected Transaction() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public Transaction(double transactionAmount, Date transactionDate, User fromUserId, User toUserId,
			String transactionNotes) {
		super();
		this.transactionAmount = transactionAmount;
		this.transactionDate = transactionDate;
		this.fromUserId = fromUserId;
		this.toUserId = toUserId;
		this.transactionNotes = transactionNotes;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public double getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public User getFromUser() {
		return fromUserId;
	}

	public void setFromUser(User fromUser) {
		this.fromUserId = fromUser;
	}

	public User getToUser() {
		return toUserId;
	}

	public void setToUser(User toUser) {
		this.toUserId = toUser;
	}

	public String getTransactionNotes() {
		return transactionNotes;
	}

	public void setTransactionNotes(String transactionNotes) {
		this.transactionNotes = transactionNotes;
	}

	/**
	 * Create a clone from transaction without password
	 * @return
	 */
	public Transaction cloneTransactionWithoutPassword() {

		return new Transaction(this.transactionId, this.transactionAmount, this.transactionDate,
				this.fromUserId.cloneUserWithoutPassword(), this.toUserId.cloneUserWithoutPassword(),
				this.transactionNotes);
	}

}
