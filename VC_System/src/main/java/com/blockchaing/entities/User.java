package com.blockchaing.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "users")
public class User implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	long userId;
	
	String username;
	String email;
	String password;
	double accountBalance;
	
	@OneToMany(mappedBy = "fromUserId", fetch = FetchType.LAZY)
	@JsonIgnore
	private Set<Transaction> fromTransactions;
	
	@OneToMany(mappedBy = "toUserId")
	@JsonIgnore
	private Set<Transaction> toTransactions;
	
	
	public User() {
		
	}
	
	
	public User(long userId) {
		super();
		this.userId = userId;
	}


	public User(String username, String email) {
		super();
		this.username = username;
		this.email = email;
	}
	
	
	

	public User( String username, String email, String password) {
		super();
		this.username = username;
		this.email = email;
		this.password = password;
	}

	
	public User(String username, String email, String password, double accountBalance) {
		super();
		this.username = username;
		this.email = email;
		this.password = password;
		this.accountBalance = accountBalance;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public Set<Transaction> getFromTransactions() {
		return fromTransactions;
	}

	public void setFromTransactions(Set<Transaction> fromTransactions) {
		this.fromTransactions = fromTransactions;
	}

	public Set<Transaction> getToTransactions() {
		return toTransactions;
	}

	public void setToTransactions(Set<Transaction> toTransactions) {
		this.toTransactions = toTransactions;
	}

	
	

	protected User(long userId, String username, String email, double accountBalance) {
		super();
		this.userId = userId;
		this.username = username;
		this.email = email;
		this.accountBalance = accountBalance;

	}

	/**
	 * create a clone from User Object with password null
	 * @return
	 */
	public User cloneUserWithoutPassword() {
		
		User user = new User(this.userId,this.username, this.email, this.accountBalance);
		return user;
	}


	@Override
	public boolean equals(Object obj) {
		User user = (User) obj;
		if(user.userId != 0 && this.userId != 0 && user.userId == this.userId)
			return true;
		return false;
	}
	
	
	
	
	
	
	
	
	

}
