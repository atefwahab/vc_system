package com.blockchaing.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.blockchaing.entities.User;
import com.blockchaing.exceptions.UserNotFoundError;
import com.blockchaing.repositories.UserRepository;

@Service
public class UserService {

	private static final Logger logger = LoggerFactory.getLogger(UserService.class);

	@Autowired
	UserRepository userRepository;

	/**
	 * this method create a user and returns its id
	 * 
	 * @param username
	 * @param email
	 * @param password
	 * @return
	 */
	public User createUser(String username, String email, String password) {

		User user = new User(username, email, password, 0);
		userRepository.save(user);

		logger.info("user id is >>>> " + user.getUserId());
		return user;
	}

	public long getUsersCount() {

		return userRepository.count();
	}

	/**
	 * This method add desired amount of money to all registered accounts in system
	 * 
	 * @param amount
	 * @return
	 */
	@Transactional
	public boolean addAmountToAllUsers(double amount) {

		Iterable<User> usersIterable = userRepository.findAll();
		// add amount for each user
		usersIterable.forEach(user -> user.setAccountBalance(user.getAccountBalance() + amount));

		userRepository.saveAll(usersIterable);
		return true;
	}

	/**
	 * This method is used to add amount of money to account balance
	 * 
	 * @param user
	 * @param amount
	 * @return
	 */
	@Transactional
	public boolean addAmountToAccountBalance(User user, double amount) {

		user.setAccountBalance(user.getAccountBalance() + amount);
		userRepository.save(user);
		return true;
	}

	/**
	 * This method is used to add amount of money to account balance
	 * 
	 * @param userId
	 * @param amount
	 * @return
	 */
	@Transactional
	public boolean addAmountToAccountBalance(Long userId, double amount) {

		Optional<User> userOptional = userRepository.findById(userId);

		if (userOptional.isPresent()) {
			return this.addAmountToAccountBalance(userOptional.get(), amount);
		}

		throw new UserNotFoundError();
	}

	/**
	 * This method is used to deducte amount of money from account balance
	 * 
	 * @param user
	 * @param amount
	 * @return
	 */
	@Transactional
	public boolean deducteAmountFromAccountBalance(User user, double amount) {

		user.setAccountBalance(user.getAccountBalance() - amount);
		userRepository.save(user);
		return true;
	}

	/**
	 * This method is used to deducte amount of money from account balance
	 * 
	 * @param userId
	 * @param amount
	 * @return
	 */
	@Transactional
	public boolean deducteAmountFromAccountBalance(Long userId, double amount) {

		Optional<User> userOptional = userRepository.findById(userId);

		if (userOptional.isPresent()) {
			return this.deducteAmountFromAccountBalance(userOptional.get(), amount);
		}

		throw new UserNotFoundError();

	}

	/**
	 * @author atefwahab This method fetch user using sent user name
	 * @param username
	 * @return
	 */
	public User getUserByUserName(String username) {

		return userRepository.findUserByUsername(username);
	}

	/**
	 * @author atefwahab This method used to get user data using it unique id
	 * @param userId
	 * @return
	 */
	@Transactional
	public Optional<User> getUserById(Long userId) {

		return userRepository.findById(userId);
	}

	/**
	 * @author atefwahab To find all users except me
	 * @param userId
	 * @return
	 */
	@Transactional
	public List<User> getOtherUsers(Long userId) {

		List<User> users = new ArrayList<User>();

		Iterable<User> usersIterable = userRepository.findByUserIdNot(userId);

		usersIterable.forEach(user -> users.add(user.cloneUserWithoutPassword()));

		return users;

	}

	/**
	 * This method is used to return list of users by list of ids
	 * 
	 * @param userIds
	 * @return
	 */
	@Transactional
	public Iterable<User> getUsersByIds(Iterable<Long> userIds) {
		return userRepository.findAllById(userIds);
	}
}
