/**
 * 
 */
package com.blockchaing.services;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.blockchaing.entities.Transaction;
import com.blockchaing.entities.User;
import com.blockchaing.exceptions.MoreThanTenTransactions;
import com.blockchaing.exceptions.UserNotFoundError;
import com.blockchaing.exceptions.UsersNotSelectForTransaction;
import com.blockchaing.exceptions.YourBalanceIsNotEnough;
import com.blockchaing.repositories.TransactionRepository;

/**
 * @author atefwahab
 *
 */
@Service
public class TransactionService {

	@Autowired
	TransactionRepository transactionRepository;
	
	@Autowired 
	UserService userService;
	
	@Transactional
	public List<Transaction> getAllTransactions(){
		Iterator<Transaction> iterator = transactionRepository.findAll().iterator();
		List<Transaction> transactions = new ArrayList<Transaction>();
		while(iterator.hasNext()) {
			
			transactions.add(iterator.next());
		}
		return transactions;
		
	}
	
	
	/**
	 * This method used to return transaction where user to transfer money from his account 
	 * and it receives userId of user
	 * @param fromUserId
	 * @return
	 */
	@Transactional
	public List<Transaction> getFromTransactions(Long fromUserId){
		
		List<Transaction> transactions = new ArrayList<Transaction>();
		
		
				
				Iterable<Transaction> transactionsIterable =transactionRepository.findTransactionByFromUserIdSorted(fromUserId);
				transactionsIterable.forEach(transaction -> {
					transactions.add(transaction.cloneTransactionWithoutPassword());
				});
			
			return transactions;
	}


/**
 * This method is used to get transaction where user received money to his account 
 * and it receives userId of user
 * @param toUserId
 * @return
 */
	@Transactional
	public List<Transaction> getToTransactions(Long toUserId){
	
	List<Transaction> transactions = new ArrayList<Transaction>();
	
		
			
			Iterable<Transaction> transactionsIterable =transactionRepository.findTransactionByToUserIdSorted(toUserId);
			transactionsIterable.forEach(transaction -> {
				transactions.add(transaction.cloneTransactionWithoutPassword());
			});
		
		return transactions;
}
	
	/**
	 * This method should to insert a transaction 
	 * @param transactionAmount
	 * @param fromUserId
	 * @param toUserId
	 * @param transactionNotes
	 * @return
	 */
	@Transactional
	public boolean createTransaction(double transactionAmount, User fromUserId, User toUserId,
			String transactionNotes) {
		Date transactionDate = new Date(new java.util.Date().getTime());
		Transaction transaction = new Transaction(transactionAmount, transactionDate, fromUserId, toUserId, transactionNotes);
		
		transactionRepository.save(transaction);
		return true;
	}
	
	/**
	 * This method used to transfer money from user to other user up to 10 users at the same time
	 * and it is  do the basic validation
	 * @param transactionAmount
	 * @param fromUserId
	 * @param toUserIds
	 * @param transactionNotes
	 * @return
	 */
	@Transactional
	public boolean transferMoney(double transactionAmount, Long fromUserId, List<Long> toUserIds,
	String transactionNotes) {
		
		 User fromUser;
		
		// validation if there is no to users 
		if(toUserIds == null || toUserIds.size()==0)
			throw new UsersNotSelectForTransaction();
			
		
		// checking users length -- user should be able to send to up 10 
		if(toUserIds.size()>10)
			throw new  MoreThanTenTransactions();
		
		// checking if user balance is enough for transactions
		double totalTransferAmount = transactionAmount * toUserIds.size();
		
		//  fetching user balance 
		Optional<User> userOptional = userService.getUserById(fromUserId);
		if(userOptional.isPresent() ) {
			
			fromUser = userOptional.get();
			
			// user balance is not enough for transfer 
			if(userOptional.get().getAccountBalance() < totalTransferAmount)
				throw new YourBalanceIsNotEnough();
			
			
		}else {
			
			// in case user is not found in db
			throw new UserNotFoundError();
		}
		
		Iterable<User> toUsers = userService.getUsersByIds(toUserIds);
		
		toUsers.forEach(toUser -> {
			
			// make a single transaction 
			// 1- deduct from user balance
				userService.deducteAmountFromAccountBalance(fromUser, transactionAmount);
				
			// 2- add to user balance 
				userService.addAmountToAccountBalance(toUser, transactionAmount);
				
			// 3- record transaction 
				this.createTransaction(transactionAmount, fromUser, toUser, transactionNotes);
		});
		
		return true;
		
	}
}
