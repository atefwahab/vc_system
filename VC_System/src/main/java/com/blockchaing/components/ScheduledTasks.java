/**
 * 
 */
package com.blockchaing.components;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.blockchaing.services.UserService;

/**
 * @author atefwahab
 *
 */

@Component
public class ScheduledTasks {
	
	
	Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);
	

	@Autowired
	UserService userService;


	/**
	 * @author atefwahab
	 * This method is scheduled Automatic to accrue user accounts 0.25 VC every 30 minutes

	 */
	@Scheduled(initialDelay = 30 * 60 * 1000,fixedRate = 30 * 60 * 1000)
	public void addAmountToUsersAtScheduledTime() {
		
		userService.addAmountToAllUsers(0.25);
		logger.info("All user accounts accrue 0.25  @ [ "+new Date().toString()+" ]");
		
	}

}
