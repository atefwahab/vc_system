package com.blockchaing.restControllers;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.blockchaing.entities.Transaction;
import com.blockchaing.entities.User;
import com.blockchaing.exceptions.UserNotFoundError;
import com.blockchaing.jsonDAOS.TransactionalList;
import com.blockchaing.repositories.UserRepository;
import com.blockchaing.services.TransactionService;
import com.blockchaing.services.UserService;

@RestController
public class MainRestController extends BaseRestController {

	public static final Logger logger = LoggerFactory.getLogger(MainRestController.class);
	// TODO should be removed from controller
	@Autowired
	UserRepository userRepository;

	@Autowired
	UserService userService;
	@Autowired
	TransactionService transactionService;
	






	/**
	 * @author atefwahab this method used to authenticate user and it returns user
	 *         object in case of authentication and null in case invalid user name
	 *         or password
	 * @param newUser
	 * @return
	 */
	@PostMapping("/authenticate")
	public User authenticateUser(@RequestBody User newUser) {

		logger.info("username sent is => " + newUser.getUsername());
		// validation of sent user name and password
		if (newUser.getUsername() != null && !newUser.getUsername().isEmpty() && newUser.getPassword() != null
				&& !newUser.getPassword().isEmpty()) {

			User user = userService.getUserByUserName(newUser.getUsername());
			if (user != null && user.getPassword().equals(newUser.getPassword()))
				return user.cloneUserWithoutPassword();

		}

		throw new UserNotFoundError();

	}

	/**
	 * This API is used to create new user with balance 0 and Database will provide
	 * userId
	 * 
	 * @param newUser
	 * @return user if there is no error and return null if there is an error
	 */
	@PostMapping("/createUser")
	public User createUser(@RequestBody User newUser) {

		logger.info("username sent is => " + newUser.getUsername());
		// validation of sent user e and password
		if (newUser.getUsername() != null && !newUser.getUsername().isEmpty() && newUser.getPassword() != null
				&& !newUser.getPassword().isEmpty()) {

			// checking if user Already exists
			if (userService.getUserByUserName(newUser.getUsername()) != null)
				return null;

			User user = userService.createUser(newUser.getUsername(), newUser.getEmail(), newUser.getPassword());
			if (user != null)
				return user.cloneUserWithoutPassword();

		}

		return null;

	}

	/**
	 * This method used to the main information of user like username, email, and
	 * account balance
	 * 
	 * @param userId
	 * @return
	 */
	@GetMapping("/getUserData")
	public User getUserbyId(@RequestHeader(name = "userId") Long userId) {

		Optional<User> user = userService.getUserById(userId);
		if (user.isPresent())
			return user.get().cloneUserWithoutPassword();
		
		throw new UserNotFoundError();

	}

	/**
	 * This method used to return transaction where user to transfer money from his account 
	 * and it receives userId of user
	 * @param fromUserId
	 * @return
	 */
	@GetMapping("/getTransactionFromAccount")
	public List<Transaction> getTransactionFromAccount(@RequestHeader(name = "userId") Long fromUserId) {

		return transactionService.getFromTransactions(fromUserId);
	}

	/**
	 * This method is used to get transaction where user received money to his
	 * account and it receives userId of user
	 * 
	 * @param toUserId
	 * @return
	 */
	@GetMapping("/getTransactionToAccount")
	public List<Transaction> getTransactionsToAccount(@RequestHeader(name = "userId") Long toUserId) {
		return transactionService.getToTransactions(toUserId);

	}
	
	
	/**
	 * @author atefwahab
	 * this method is used to get all users on system except me 
	 * @param toUserId
	 * @return
	 */
	@GetMapping("/getOtherUsers")
	public List<User> getOtherUsers(@RequestHeader(name = "userId") Long userId){
		
		return userService.getOtherUsers(userId);
		
	}
	
	/**
	 * @author atefwahab
	 * This service used to transfer money to users up to 10 users at the same time
	 * it returns 1 if it is success and error if there is an error
	 * @param transactionalList
	 * @param userId
	 * @return
	 */
	@PostMapping("/transfer")
	public int transferMoney(@RequestBody TransactionalList transactionalList,@RequestHeader(name = "userId") Long userId) {
		
		List<Long> toUsers = transactionalList.getToTransferUserIds();
		double transactionAmount = transactionalList.getTransferAmount();
		String transactionNotes = transactionalList.getTransferNotes();
		
		// TODO transfer Logic 
		transactionService.transferMoney(transactionAmount, userId, toUsers, transactionNotes);
		
		return 1;
	}
}
