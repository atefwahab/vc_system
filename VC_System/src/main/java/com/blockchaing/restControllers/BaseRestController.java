/**
 * 
 */
package com.blockchaing.restControllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author atefwahab
 * A marker class used to know the base of api 
 */

@RestController
@RequestMapping("/api")
public abstract class BaseRestController {

}
