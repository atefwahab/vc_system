/**
 * 
 */
package com.blockchaing.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author atefwahab
 * error should be thrown if user didn't choose any user for transaction
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR,reason = "You should select at least one user for transaction" )
public class UsersNotSelectForTransaction extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5187362781002965771L;

}
