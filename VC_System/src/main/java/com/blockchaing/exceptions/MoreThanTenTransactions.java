/**
 * 
 */
package com.blockchaing.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author atefwahab
 * error should be thrown in case user tried to do more than 10 transaction at the same time 
 *
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR,reason = "User is able to sent  up to only 10 different users at a time" )
public class MoreThanTenTransactions extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
