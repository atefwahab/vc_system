/**
 * 
 */
package com.blockchaing.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author atefwahab
 * an error will be thrown in case there is no enough balance 
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR,reason = "User balance is not enough for transactions" )
public class YourBalanceIsNotEnough extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5130121703346601939L;

}
