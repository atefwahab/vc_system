/**
 * 
 */
package com.blockchaing.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author atefwahab
 * Error should be thrown in case user is not found 
 *
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "User not found!")
public class UserNotFoundError extends RuntimeException {

	
	private static final long serialVersionUID = 2140065607299855649L;

}
