package com.blockchaing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.blockchaing.services.UserService;

@SpringBootApplication
@EnableScheduling
public class VcSystemApplication implements CommandLineRunner {

	 final static Logger logger = LoggerFactory.getLogger(VcSystemApplication.class);
	
	@Autowired
	UserService userService;

	public static void main(String[] args) {
		SpringApplication.run(VcSystemApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		// create dummy 11 users in beginning of application
		createDummyUsers();

	}

	/**
	 * this method create a dummy 11 users each user has username = user_[1:11]
	 * email = user_[1:11]@@blockchaingroup.com
	 * password = 1234
	 */
	private void createDummyUsers() {
		
		logger.info("count is >> "+userService.getUsersCount());
		if (userService.getUsersCount() == 0) {
			for (int i = 1; i <= 11; i++) {

				userService.createUser("user_" + i, "user_" + i + "@blockchaingroup.com", "1234");
			}
		}
	}

}
