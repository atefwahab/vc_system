/**
 * 
 */
package com.blockchaing.jsonDAOS;

import java.io.Serializable;
import java.util.List;

/**
 * This Class will be used as a representation to how json object sent from api 
 * should be
 * @author atefwahab
 *
 */


public class TransactionalList implements Serializable{


	private static final long serialVersionUID = 1L;
	
	private List<Long> toTransferUserIds;
	private double transferAmount;
	private String transferNotes;
	
	
	
	
	public TransactionalList() {

	}
	public TransactionalList(List<Long> toTransferUserIds, double transferAmount) {
		super();
		this.toTransferUserIds = toTransferUserIds;
		this.transferAmount = transferAmount;
	}
	public List<Long> getToTransferUserIds() {
		return toTransferUserIds;
	}
	public void setToTransferUserIds(List<Long> toTransferUserIds) {
		this.toTransferUserIds = toTransferUserIds;
	}
	public double getTransferAmount() {
		return transferAmount;
	}
	public void setTransferAmount(double transferAmount) {
		this.transferAmount = transferAmount;
	}
	public String getTransferNotes() {
		return transferNotes;
	}
	public void setTransferNotes(String transferNotes) {
		this.transferNotes = transferNotes;
	}
	
	
	

}
