package com.blockchaing.controllers;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	
	public static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	 @RequestMapping(value = "/{path:[^\\.]*}")
	    public String redirect() {
	        return "redirect:/";
	    }
	
	@RequestMapping(value  = "/")
	public String index() {
		logger.info("Home Controll was Called @"+new Date().toString());
		return "index";
	}

}
