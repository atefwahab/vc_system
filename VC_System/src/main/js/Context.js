import React, { useContext, createContext, useState } from "react";

//default values for context 

// create context which can be shared within components
export const authContext = createContext({isLoggedIn: false});


export const ProvideAuth = ({ children })=> {
    const [isLoggedIn,setIsLoggedIn] = useState(false);
    const [userId, setUserId] = useState();

	const login = (userId)=>{

        console.log(userId);
        setIsLoggedIn(true);
        setUserId(userId);
	}
	const logout = () => {
        setIsLoggedIn(false);
        setUserId(null);
	}
  // const auth = useProvideAuth();
  const auth = false;
  return <authContext.Provider value={{
    isLoggedIn: isLoggedIn ,
    login,
    logout: logout,
    userId
}} >{children}</authContext.Provider>;
}


export const AuthenicationContext =  () => {
    return useContext(authContext);
  }
