"use strict";

import React, { useContext, createContext, useState } from "react";
const ReactDOM = require("react-dom"); 
import {
  BrowserRouter as Router,
  Switch,
  Route,

  Redirect,
  useHistory,
  useLocation,
} from "react-router-dom";
import Login from "./components/Login";
import CreateAccount from './components/CreateAccount';
import Home from './components/Home';
import {Styles} from './styles/Styles';

import { AuthenicationContext,ProvideAuth} from './Context';

const App = props => {


   // redirect user to the right page
   let history = useHistory();
   let auth = AuthenicationContext();
 
   

    // handle routing in application

    return (
      <ProvideAuth>
        <Router>
          <div style={Styles}>

        
          <Route exact path="/">
          {auth.isLoggedIn ? <Redirect to="/home" /> : <Login />}
        </Route>

              <Switch>
                <Route path="/Login">
                  <Login />
                </Route>
                <Route path="/CreateAccount">
                  <CreateAccount />
                </Route>
                <PrivateRoute path="/home">
                  <Home />
                </PrivateRoute>
              </Switch>
     
          </div>
        </Router>
      </ProvideAuth>
    );

}

// render main Component in index page
ReactDOM.render(<App />, document.getElementById("react"));




function Profile() {
  return (
    <div>
      <h2>Profile Page</h2>
    </div>
  );
}

function Transfer() {
  return (
    <div>
      <h2>transfer Page</h2>
    </div>
  );
}


function LoginO() {
	let history = useHistory();
	let location = useLocation();
	let { from } = location.state || { from: { pathname: "/" } };
	let auth = AuthenicationContext();
  console.log(from);
  console.log(auth);
  console.log(auth.isLoggedIn);
	const login = () => {
		auth.login(50);
		history.replace(from);
	}
	return (
		<div>
		  <p>You must log in to view the page at {from.pathname}</p>
		  <button onClick={login}>Log in</button>
		</div>
	  );
  }


function PrivateRoute({ children, ...rest }) {
  let auth = AuthenicationContext();
  return (
    <Route
      {...rest}
      render={({ location }) =>
        auth.isLoggedIn ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
}
