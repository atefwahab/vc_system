import React from 'react';

 export const Styles =  {

   mainTitle: {
      textDecoration: 'underline',
      backgroundColor: '#4e5a70',
      display: 'inline-block',
      margin: '15px',
   },
   TransferContainer:{
      marginBottom: '50px',
   },

    backgroundColor: '#121d33',
    table:{
       border: '1px solid #fff',
       thead: {
          backgroundColor: '#fff',
          color: '#0c6cf2'
       },
       th: {
          minWidth: 250,
          border: '1px black solid',
          textAlign: 'center',

       },
       td: {
         borderRight: '1px #fff solid',
         backgroundColor: '#ced9ef52',
         textAlign: 'center',
       },
       tr:{
         border: '1px solid #fff',
       }
    }


 }

 export const InputStyles = {
    loginBox: {
       border: '1px solid #fff',
       borderRadius: '20px',
       margin: 'auto',
       maxWidth: '400px',
       backgroundColor: '#fff',
       color: '#121d33',


    },
    ulList:{
      
   listStyleType: 'none',
   margin: '0px',
   padding: '0',
   overflow: 'hidden',
   backgroundColor: '#fff',
    },
    liList: {
       float:'left',
       marginRight: '20px',
    },
    listLink: {

      display: 'block',
      color: '#121d33',
      textAlign: 'center',
      padding: '16px',
      fontWeight: 'bold',
      textDecoration: 'none',
      fontSize: '25px',
    },
    inputChild: {
       marginLeft: '50px'
    },
    marginBottom : '20px',
    linkStyle: {
      
      color: '#0c6cf2',
      marginTop: '20px',
      padding: '10px',
      textDecoration: 'none',
      marginLeft: '70px',
    },
    inputLabel:{display: 'block'},
    inputButton:{
      display: 'inline-block',
      fontWeight: 600,
      textAlign: 'center',
      outline: 'none',
      transition: 'all 0.3s ease 0s',
      borderRadius: '8px',
      height: '2.25rem',
      color: '#fff',
      opacity: 1,
      fontSize: '0.85rem',
      border: 'transparent',
      backgroundColor: '#0c6cf2',
      lineHeight: '2.5rem',
      minWidth: '120px',
      marginBottom: '15px',
      marginLeft: '80px',
    }
 }