import React,{useState,useEffect} from 'react';
import {AuthenicationContext} from '../Context'
import TransactionsTable from './TransactionsTable';


import {
    useHistory
  } from "react-router-dom";

  import {Styles} from '../styles/Styles'


const Profile = props => {

    // states
    const [isUserLoaded,setIsUserLoaded] = useState(false);
    const [user,setUser] = useState({});
    const [fromTransactions,setFromTransactions] = useState([]);
    const [toTransactions,setToTransactions] = useState([]);

    const history = useHistory();
    const authContext = AuthenicationContext();
    const userId = authContext.userId;
    
    const signOut = () => {

        
        authContext.logout();
        history.push("/");
    }

    // fetch user data 
    const getUserInfo = async () => {

      const requestOptions = {method: 'GET',
      headers: { 'Content-Type': 'application/json' , 
      userId: userId},}
      fetch('api/getUserData',requestOptions)
      .then(  async response => { 
        
         let res = await response.json();
         if(!response.ok){
           throw new Error(response.status+' '+res.message);
         }
         return res;
        
        })
      .then(
        (result) => {
         
          setUser(result);
          setIsUserLoaded(true);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          console.error(error);
        }
      )
    }




    const getFromTransactions = async () => {

      const requestOptions = {method: 'GET',
      headers: { 'Content-Type': 'application/json' , 
      userId: userId},}
      fetch('api/getTransactionFromAccount',requestOptions)
      .then(  async response => {
        
        let res = await response.json()
        if(!response.ok){
          throw new Error(response.status+' '+res.message);
        }
        return res;
      })
      .then(
        (result) => {
          
          setFromTransactions(result);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          console.error(error);
        }
      )
    }


    const getToTransactions = async () => {

      const requestOptions = {method: 'GET',
      headers: { 'Content-Type': 'application/json' , 
      userId: userId},}
      fetch('api/getTransactionToAccount',requestOptions)
      .then(  async response => {

        let res = await response.json()
        if(!response.ok){
          throw new Error(response.status+' '+res.message);
        }
        return res;
      })
      .then(
        (result) => {
         
          setToTransactions(result);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          console.error(error);
        }
      )
    }

    // Loading from Transactions 
    useEffect(()=>{
        getFromTransactions();
        getToTransactions();
    },[user])
	
  
     const getUser =  useEffect(()=>{

         getUserInfo();
     },[]);
  

     if(isUserLoaded){

      const {username, email , accountBalance} = user;
       // every thing is fine and user data is loaded 
       return (
        <div>
          <div>
            <h2 style={Styles.mainTitle}>User Information</h2>
            <div style={{
                  border: '1px solid',
                  borderRadius: '5px',
                  padding: '20px',
                  display: 'block'
            }}>
              <div>
                <h3>Username:  <span style={{color: '#0c99f6'}}>{username}</span></h3>
               
              </div>
  
              <div>
                <h3>Email:  <span style={{color: '#0c99f6'}}>{email}</span></h3>
                
              </div>
  
              <div>
                <h3>Account Balance:  <span style={{color:'#81b562'}}>${accountBalance}</span></h3>
                
              </div>
            </div>
          </div>
  
          {/* end of user Information */}
          <div>
            <h2 style={Styles.mainTitle}>User Transactions</h2>
            {/* transaction from my account */}
             <TransactionsTable  transactions={fromTransactions} isTo={true} title="Transactions from your account"/>
             {/* transaction to my account */}
             <TransactionsTable  transactions={toTransactions} isTo={false} title="Transactions to your account"/>
          </div>
        </div>
      );
     }else {

      return(<div>user data is loading ...</div>);
     }
	
	
}





export default Profile;