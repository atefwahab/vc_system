import React,{useState} from 'react';
import {AuthenicationContext} from '../Context';

import {
	useHistory,
	useLocation,
	Link
  } from "react-router-dom";

  import {InputStyles} from '../styles/Styles'
const Login = props => {
	
	const [username,setUsername] = useState("");
	const [password,setPassword] = useState("");
	const [isError,setIsError] = useState(false);

	const authContext = AuthenicationContext();
	const history = useHistory();
	const location = useLocation();
	let { from } = location.state || { from: { pathname: "/" } };
	
	
	 const handleUsernameChange =event => {
		
		
		   setUsername(event.target.value)
		  }
	 
	 
	 const handlePasswordChange =(event) => {
		
		   setPassword(event.target.value);
		  }
	 
	 const handleLoginSubmit = (event)=>{
		
		
		
		 authenticateUser();
		 event.preventDefault();
	 }
	
	const  authenticateUser = async ()=> {
		
	
	
		const requestOptions = {
		        method: 'POST',
		        headers: { 'Content-Type': 'application/json' },
		        body: JSON.stringify({ username: username, password: password })
		    };
		
		    fetch('/api/authenticate', requestOptions).then(async response => {
					
				console.log(response);
				if(!response.ok){
					let error = await response.json();
					throw new Error(response.status+': '+error.message  );
				}
		        	// authentication logic
					let res = await response.json();
					
				
		        	return res
		        })
				.then(data => { 


					console.log(data);
					
					setIsError(false);
					authContext.login(data.userId);
					history.push("/home");
					
					}, error => {setIsError(true)
					console.error(error)});
		}
	
	 
		 
		 
		
		
		return (<form  onSubmit={handleLoginSubmit} >

			<div style={InputStyles.loginBox}>
			<div style={InputStyles.inputChild}>
				<h1>Login to your account</h1>
			</div>
		{isError && (<div> <h1 style={{color: 'red'}}>invalid username or password!</h1> </div>) } 
       <div style={InputStyles.inputChild}>
		<label style={InputStyles.inputLabel}>
        Username:
        <input style={InputStyles} type="text" placeholder="enter username"  value={username} onChange={handleUsernameChange}/>
      </label>
        </div>
        
        <div style={InputStyles.inputChild}>
        
        <label style={InputStyles.inputLabel}>
        Password:
        <input style={InputStyles} type="password" placeholder="enter password"   value={password} onChange={handlePasswordChange}/>
      </label>
        </div>
		<div style={InputStyles.inputChild}>
	  <input style={{...InputStyles.inputButton}} type="submit" value="Login" />
	  </div>
	   <div style={InputStyles.inputChild}>
	   <Link style={{...InputStyles.linkStyle}} to="/CreateAccount" >Create New Account</Link>
	   </div>
	   </div>
    </form>);
	
}

//Login.contextType = authContext;

export default Login