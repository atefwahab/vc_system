import React,{useState} from 'react';
import {AuthenicationContext} from '../Context';
import {InputStyles} from '../styles/Styles'

import {
	useHistory,
	Link
  } from "react-router-dom";
const CreateAccount = props => {
	
	const [username,setUsername] = useState("");
	const [email,setEmail] = useState("");
	const [password,setPassword] = useState("");
	const [isError,setIsError] = useState(false);

	const authContext = AuthenicationContext();
	const history = useHistory();
	let { from } = { from: { pathname: "/home" } };
	
	
	 const handleUsernameChange =event => {
		
			
		   setUsername(event.target.value)
		  }


		  const handleEmailChange =event => {
		
		
			setEmail(event.target.value)
		  }
	 
	 
	 
	 const handlePasswordChange =(event) => {
		
		   setPassword(event.target.value);
		  }
	 
	 const handleLoginSubmit = (event)=>{
		
		
		createUser();
		 event.preventDefault();
	 }
	
	const  createUser = async ()=> {
		
		
	
		const requestOptions = {
		        method: 'POST',
		        headers: { 'Content-Type': 'application/json' },
		        body: JSON.stringify({ username, email ,password })
		    };
		
		    fetch('/api/createUser', requestOptions).then(async response => {
		        	
		        	// authentication logic
					let res = await response.json();
					if(!response.ok){
						throw new Error(response.status+ '  '+res.message);
					}
		        	return res
		        })
				.then(data => { 

					setIsError(false);
					authContext.login(data.userId);
					history.push("/home");
					
					}, error => {setIsError(true)});
		}
	
	 
		 
		 
		
		
		return (<form  onSubmit={handleLoginSubmit} >
			<div style={InputStyles.loginBox}>
			<div style={InputStyles.inputChild}>
				<h1>Create Account</h1>
			</div>
		{isError && (<div> <h1 style={{color: 'red'}}>empty username or password Or Username alreay exists!</h1> </div>) } 

       <div style={InputStyles.inputChild}> <label style={InputStyles.inputLabel}>
        User Name:
        <input style={InputStyles} type="text" placeholder="enter username"  value={username} onChange={handleUsernameChange}/>
      </label>
        </div>

		<div style={InputStyles.inputChild}> <label style={InputStyles.inputLabel}>
        email:
        <input style={InputStyles} type="email" placeholder="enter email"  value={email} onChange={handleEmailChange}/>
      </label>
        </div>


		
        
        <div style={InputStyles.inputChild}>
        
        <label style={InputStyles.inputLabel}>
        Password:
        <input style={InputStyles} type="password" placeholder="enter password"   value={password} onChange={handlePasswordChange}/>
      </label>
        </div>
      <input  style={{...InputStyles.inputButton}} type="submit" value="Create Account" />
	  <div >
	  <Link style={{...InputStyles.linkStyle}} to="/Login">Login Page</Link>
	  </div>
	  </div>
    </form>);
	
}

//Login.contextType = authContext;

export default CreateAccount