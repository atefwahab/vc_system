import React from 'react';
import {Styles} from '../styles/Styles';

const TransactionsTable = (props)=> {


    return(
        <div>
                <h3>{props.title}</h3>
                <div>
                  <table style={Styles.table}>
                  
                    <thead style={Styles.table.thead}>
                      <tr>
                      <th style={Styles.table.th}>Transaction Id</th>
                      <th style={Styles.table.th}>Transaction Amount</th>
                      <th style={Styles.table.th}>{props.isTo? 'To': 'From'}</th>
                      <th style={Styles.table.th}>Transaction Date</th>
                      <th style={Styles.table.th}>Transaction Notes</th>
                    </tr>

                    </thead>

            <tbody>
                    {
                      props.transactions.map(transaction => { 
                        const {transactionId,transactionAmount,toUser,transactionDate,transactionNotes} = transaction;
                        return (
                          <tr style={Styles.table.tr} key={transactionId}>
                          <td style={Styles.table.td}>{transactionId}</td>
                          <td style={Styles.table.td}><span style={{color:'#afaeff'}}>${transactionAmount}</span></td>
                          <td style={Styles.table.td}>{toUser.username}</td>
                          <td style={Styles.table.td} >{transactionDate}</td>
                          <td style={Styles.table.td} >{transactionNotes}</td>
                        </tr>
                        );
                      })
                    }

                    </tbody>
                  </table>
                </div>
             </div>
    );
}
export default TransactionsTable;