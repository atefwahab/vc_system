import React,{useState,useEffect} from 'react';
import Select from "react-select";
import {AuthenicationContext} from '../Context';
import {Styles,InputStyles} from '../styles/Styles';



const Transfer = (props) => {

    const [selected, setSelected ] = useState([]);
    const [otherUsers,setOtherUsers] = useState([]);
    const [isError,setIsError] = useState(false);
    const [errorText, setErrorText] = useState('');
    const [notes, setNotes ] = useState("");
    const [amount,setAmount] =useState(0);
    const [successText, setSuccessText] = useState(null);
    

    const authContext = AuthenicationContext();
    const userId = authContext.userId;

    /**
     * This method call transfer api
     */
    const transferMoney = async () => {

        let toUserIds = selected.map(user => user.value);
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' , 
            userId: userId
        },
        body: JSON.stringify({ toTransferUserIds: toUserIds, transferAmount: amount, transferNotes : notes  })
    }

     // calling it 
     fetch('api/transfer',requestOptions).then( async response => {

        const res = await response.json();
        if(!response.ok){

            setIsError(true);
            setErrorText(res.message);
            throw new Error(response.status+' '+res.message);
        }
        setIsError(false);
        setSuccessText('Succssful transaction');
        return res;
     }).then(result => {
         console.log(result);
     },error =>{

         console.error(error);
     })

}


    const getOtherUsers = async () => {

        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' , 
            userId: userId
        }
    }
      fetch('api/getOtherUsers',requestOptions)
      .then(  async response => {
          let res = await response.json()

          if(!response.ok){
              throw new Error(response.status+' '+res.message);
          }
          return res;
        
        })
      .then(
        (result) => {
          
            console.log(result);
            setOtherUsers(result.map(user => ({value: user.userId, label: user.username}) ));
        },

        (error) => {
          console.error(error);
        }
      )
    }



    // fetching other users
    useEffect( () => {

         getOtherUsers();
    },[]);

     // handling form controls
     const handleOnSubmit = (event)=>{
		
		
		
        transferMoney();
        event.preventDefault();
    }

    const handleAmountChange = (event) => {

        setAmount(event.target.value);
    }


    const handleNotesChange = (event) => {

        setNotes(event.target.value);
    }
    return(
        <div>
            <form onSubmit={handleOnSubmit}>


            
            <h2 style={Styles.mainTitle}>Transfer screen</h2>
            {isError && <h2 style={{color:'red'}}>{errorText}</h2>}
            {successText && !isError && <h2 style={{color:'green'}}>{successText}</h2>}


        <div style={{...InputStyles.loginBox,padding:'25px',maxWidth:'1000px'}}>
            <div style={Styles.TransferContainer}> 
            <label>
            Transaction Amount:
                <input style={{display:'block', width:'50px'}} onChange={handleAmountChange} value={amount} type="number" id="amout" name="amount" min="1"/>
            </label>
            </div>
            
  
        <div style={Styles.TransferContainer}>
            <Select   placeholderButtonLabel="Select User Account" 
        defaultValue={selected}
        onChange={(value , action)=>{
            if(selected.length <= 10){

                setSelected(value);
                setIsError(false);
            }else{
                setErrorText('You Can only select up to 10!!');
                setIsError(true);
            }
            
        }}
        options={otherUsers}
        isMulti={true}
            />

    </div>

    <div style={Styles.TransferContainer}>
        <label>
            Transaction Notes:       
            </label>
            <textarea style={{    width: '500px',height: '100px', display:'block'}} value={notes} onChange={handleNotesChange} id="notes" name="notes" minLength="20" placeholder="Transaction Notes">

</textarea >
            </div>
                <input style={InputStyles.inputButton} type='submit' value="Transfer" />
        </div>
            </form>
        </div>
    )
}
export default Transfer;