import React from 'react';
import {AuthenicationContext} from '../Context'
import Profile from './Profile';
import Transfer from './Transfer';

import {
    BrowserRouter as Router,
    Switch,
    Redirect,
    Route,
    Link,
    useHistory,
  } from "react-router-dom";

  import {InputStyles} from '../styles/Styles'
const Menu = props => {
    const history = useHistory();
    const authContext = AuthenicationContext();

    
    const signOut = () => {

        
        authContext.logout();
        history.push("/");
    }
	
	
  

		return (
            <Router>
            <div>
         
                   
   
                {/* menu div */}
                <div>
                         <ul style={InputStyles.ulList}>
                             <li style={InputStyles.liList}><Link style={InputStyles.listLink} to="/profile">Profile</Link></li>
      
                             <li style={InputStyles.liList}><Link style={InputStyles.listLink} to="/transfer">Transfer Money</Link></li>

                             <li style={{float: 'right'}}>                 <div>
                        <button style={{...InputStyles.inputButton,margin:'15px'}} onClick={signOut}>Sign Out</button>
                    </div></li>
                         </ul>
                     </div>

                     <Redirect to="/profile" />
         
                 <Switch>
                   <Route path="/profile">
                     <Profile />
                   </Route>

                   <Route path="/transfer">
                     <Transfer />
                   </Route>
                 </Switch>
               
             </div>
           </Router>
        );
	
}





export default Menu;